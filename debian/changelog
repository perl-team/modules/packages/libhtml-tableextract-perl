libhtml-tableextract-perl (2.15-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 14 Jun 2022 22:48:51 +0100

libhtml-tableextract-perl (2.15-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 01 Jan 2021 15:20:57 +0100

libhtml-tableextract-perl (2.15-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Florian Schlichting ]
  * Import upstream version 2.15
  * Update copyright years
  * Fix build failure with HTML::ElementTable (rt#121920)
  * Declare compliance with Debian Policy 4.1.0

 -- Florian Schlichting <fsfs@debian.org>  Mon, 18 Sep 2017 22:30:16 +0200

libhtml-tableextract-perl (2.13-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 2.13
  * Bump dh compat to level 9
  * Drop POD test build-deps
  * Update upstream copyright years
  * Add myself to (otherwise empty) Uploaders
  * Declare compliance with Debian Policy 3.9.6
  * Mark package autopkgtest-able

 -- Florian Schlichting <fsfs@debian.org>  Fri, 11 Sep 2015 23:04:25 +0200

libhtml-tableextract-perl (2.11-1) unstable; urgency=low

  [ Nicholas Bamber ]
  * Fix bug number
  * New upstream version
  * Removed patch that is no longer required
  * Updated copyright
  * Raised standards version to 3.9.2

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 27 Aug 2011 23:13:34 +0100

libhtml-tableextract-perl (2.10-4) unstable; urgency=low

  * New maintainer (Closes: #618372)
  * Upped standards version to 3.9.1
  * Refreshed home page, svn and description fields
  * Refreshed copyright, rules and added debian/source/format
  * Standardized watch file
  * Refreshed patch

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Wed, 16 Mar 2011 21:24:27 +0000

libhtml-tableextract-perl (2.10-3) unstable; urgency=low

  * Remove unconditional rmdir to make package build with Perl 5.10.0
    (Closes: #467936)
  * debian/watch: added
  * debian/control:
    + update Homepage
    + unversion the B-D-I/Suggests on libhtml-element-extended-perl as the
      version in Etch is already new enough
  * Standards-Version: 3.7.3

 -- Florian Ernst <florian@debian.org>  Sun, 02 Mar 2008 22:11:51 +0100

libhtml-tableextract-perl (2.10-2) unstable; urgency=low

  * Previously generated unquoted HTML attributes are now quoted,
    adjusting t/30_tree.t accordingly. Thanks a lot to Gunnar Wolf
    for investigation and patch (Closes: #414721)

 -- Florian Ernst <florian@debian.org>  Wed, 14 Mar 2007 01:03:11 +0100

libhtml-tableextract-perl (2.10-1) unstable; urgency=low

  * Finally hijacking this package.
  * debian/changelog: clean cruft at the end
  * debian/control:
    + add Suggests: libhtml-element-extended-perl (>= 1.17-1) for extracting
      object trees
    + adjust short and long description
    + add Build-Depends-Indep: libtest-pod-perl, libtest-pod-coverage-perl,
      libhtml-parser-perl, libhtml-element-extended-perl (>= 1.17-1) for
      'make test'
  * debian/copyright: list previous maintainer, update information
  * debian/rules: built from scratch using dh-make-perl
  * Standards-Version 3.7.2, debhelper compatibility mode 5

 -- Florian Ernst <florian@debian.org>  Tue, 26 Sep 2006 10:54:38 +0200

libhtml-tableextract-perl (2.07-0.1) unstable; urgency=low

  * Non-maintainer upload as announced.
  * New upstream release (closes: Bug#300807)
  * Finish /usr/doc transition (closes: Bug#322820)
  * Upgrade debhelper compatibility level to 4
  * dh_testversion is deprecated, use Build-Dependencies
  * Install into /usr/share/perl5/
  * Change section to perl to match override

 -- Florian Ernst <florian@debian.org>  Tue, 14 Mar 2006 17:26:48 +0100

libhtml-tableextract-perl (1.08-1) unstable; urgency=low

  * Updated to latest upstream version (closes: Bug#169700)

 -- Ross Peachey <rcp@debian.org>  Wed,  1 Jan 2003 20:54:16 +1100

libhtml-tableextract-perl (1.06-2) frozen unstable; urgency=low

  * Fixed package description (closes: Bug#114480)

 -- Ross Peachey <rcp@debian.org>  Mon, 29 Oct 2001 04:21:47 +1100


libhtml-tableextract-perl (1.06-1) unstable; urgency=low

  * Initial Debian release.

 -- Ross Peachey <rcp@debian.org>  Wed, 21 Feb 2001 15:24:49 +1100
